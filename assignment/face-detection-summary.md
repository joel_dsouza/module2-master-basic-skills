**Face detection**  also called facial detection is an artificial intelligence (AI) based computer technology used to find and identify human faces in digital images. Face detection technology can be applied to various fields -- including security, biometrics, law enforcement, entertainment and personal safety to provide surveillance and tracking of people in real time.


**How Face detection works**

The methods used in face detection can be knowledge-based, feature-based, template matching or appearance-based. Each has advantages and disadvantages:

- Knowledge-based, or rule-based methods, describe a face based on rules. The challenge of this approach is the difficulty of coming up with well-defined rules.

- Feature invariant methods -- which use features such as a person's eyes or nose to detect a face can be negatively affected by noise and light.

- Template-matching methods are based on comparing images with standard face patterns or features that have been stored previously and correlating the two to detect a face. 

- Appearance-based methods employ statistical analysis and machine learning to find the relevant characteristics of face images. This method, also used in feature extraction for face recognition, is divided into sub-methods.

[Click Here](https://miro.medium.com/max/512/1*1WEz_nmsV4L9RqyIV0x25w.png)

**Some of the more specific techniques used in face detection include:**

- Removing the background. For example, if an image has a plain, mono-color background or a pre-defined, static background, then removing the background can help reveal the face boundaries.

- In color images, sometimes skin color can be used to find faces; however, this may not work with all complexions.

- Using motion to find faces is another option. In real-time video, a face is almost always moving, so users of this method must calculate the moving area.

- A combination of the strategies listed above can provide a comprehensive face detection method.
